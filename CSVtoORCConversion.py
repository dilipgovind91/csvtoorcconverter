##########################################################################################
#This program will convert the CSV files in Input_csv_files folder
#and produce respective ORC files in Output_ORC_files folder
#Pre-requiste: 
#1. Import pyorc, pandas, pathlib, glob & os
#2. CSV file must be placed in Input_csv_files
#3. CSV file should not have column name appended with table name (e.g. clinstdy.studyid)
###########################################################################################
import pyorc
from pyorc.enums import StructRepr
import pandas as pd
from pathlib import Path
import glob
import os

path = "C:\\Users\\U1086126\\PycharmProjects\\CSV to ORC Conversion\\Input_csv_files"
csv_files = glob.glob(os.path.join(path, "*.csv"))
#csv_files = [file for file in os.listdir(path) if file.endswith('.csv')]

for fr in csv_files:
    df = pd.read_csv(fr)
    df1 = pd.read_csv(fr,dtype=str).fillna('') 
    headers = list(df1.columns)
    structure = "struct<" + ",".join(["{}:string".format(x) for x in (headers)]) + ">"
    r1 = Path("C:\\Users\\U1086126\\PycharmProjects\\CSV to ORC Conversion\\Output_ORC_files").joinpath(f'{fr}')
    file_name = Path(r1.name).stem
    extension = ".orc"
    generated_path = Path("C:\\Users\\U1086126\\PycharmProjects\\CSV to ORC Conversion\\Output_ORC_files").joinpath(f'{file_name}{extension}')
    output = open(generated_path, "wb")
    writer = pyorc.Writer(output, structure)
    for i in range(len(df1)):
        writer.write(tuple(df1.iloc[i]))
    writer.close()
    print("FileName:",file_name + ".orc","file generated", " & No.of records =", len(df1))